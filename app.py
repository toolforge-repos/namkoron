import json
import os
import random
import re
import string
import urllib.parse

import flask
import more_itertools
import mwapi
import mwoauth
import requests
import requests_oauthlib
import toolforge
import yaml

from SPARQLWrapper import SPARQLWrapper, JSON

wikidata_url = "https://www.wikidata.org"
wikidata_api_url = wikidata_url + "/w/api.php"
wikidata_index_url = wikidata_url + "/w/index.php"
query_service_url = "https://query.wikidata.org/sparql"
wd_prefix = "http://www.wikidata.org/entity/"
query_embed_prefix = "https://query.wikidata.org/embed.html#"
wkt_literal_url = "http://www.opengis.net/ont/geosparql#wktLiteral"
coord_pattern = re.compile(r"Point\((?P<lon>[0-9.E-]+) (?P<lat>[0-9.E-]+)\)")
max_obtainable_entities = 50

user_agent = toolforge.set_user_agent('namkoron','mahir256@live.com')

results_example = [{"i":{"type":"uri","value":"http://www.wikidata.org/entity/Q4115189"},"cds":{"datatype":"http://www.opengis.net/ont/geosparql#wktLiteral","type":"literal","value":"Point(-88.93 37.73028)"}},
                   {"i":{"type":"uri","value":"http://www.wikidata.org/entity/Q13406268"},"cds":{"datatype":"http://www.opengis.net/ont/geosparql#wktLiteral","type":"literal","value":"Point(-88.545833333 37.733888888)"}},
                   {"i":{"type":"uri","value":"http://www.wikidata.org/entity/Q15397819"},"cds":{"datatype":"http://www.opengis.net/ont/geosparql#wktLiteral","type":"literal","value":"Point(-89.220277777 37.726388888)"}}]
targetlang_example = 'bn'

sparql = SPARQLWrapper(query_service_url, agent=user_agent)
sparql.setReturnFormat(JSON)

app = flask.Flask(__name__)

__dir__ = os.path.dirname(__file__)
try:
    with open(os.path.join(__dir__, 'config.yaml')) as config_file:
        app.config.update(yaml.safe_load(config_file))
except FileNotFoundError:
    print('config.yaml file not found, assuming local development setup')
    app.secret_key = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(64))

if 'oauth' in app.config:
    consumer_token = mwoauth.ConsumerToken(
        app.config['oauth']['consumer_key'],
        app.config['oauth']['consumer_secret'])

def get_auth():
    access_token = mwoauth.AccessToken(**flask.session['oauth_access_token'])
    oauth_params = {
        'client_key': consumer_token.key,
        'client_secret': consumer_token.secret,
        'resource_owner_key': access_token.key,
        'resource_owner_secret': access_token.secret,
    }
    return requests_oauthlib.OAuth1(**oauth_params)

def full_url(endpoint, _external=True, **kwargs):
    if _external:
        return flask.url_for(endpoint,
            _external=True,
            _scheme=flask.request.headers.get('X-Forwarded-Proto', 'http'),
            **kwargs)
    else:
        return flask.url_for(endpoint,
            **kwargs)

@app.template_global()
def csrf_token():
    if 'csrf_token' not in flask.session:
        flask.session['csrf_token'] = ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(64))
    return flask.session['csrf_token']

@app.template_filter()
def user_link(user_name):
    return (flask.Markup(r'<a href="https://www.wikidata.org/wiki/User:') +
            flask.Markup.escape(user_name.replace(' ', '_')) +
            flask.Markup(r'">') +
            flask.Markup(r'<bdi>') +
            flask.Markup.escape(user_name) +
            flask.Markup(r'</bdi>') +
            flask.Markup(r'</a>'))

@app.template_global()
def logged_in_user_name():
    if 'user_name' in flask.g:
        return flask.g.user_name
    if 'oauth' not in app.config:
        return flask.g.setdefault('user_name', None)
    if 'oauth_access_token' not in flask.session:
        return flask.g.setdefault('user_name', None)

    access_token = mwoauth.AccessToken(**flask.session['oauth_access_token'])
    identity = mwoauth.identify(wikidata_index_url, consumer_token, access_token)
    return flask.g.setdefault('user_name', identity['username'])

@app.template_global()
def authentication_area():
    if 'oauth' not in app.config:
        return flask.Markup()

    user_name = logged_in_user_name()
    if user_name is None:
        return (flask.Markup(r'<a id="login" class="navbar-text" href="') +
                flask.Markup.escape(flask.url_for('login')) +
                flask.Markup(r'">Log in</a>'))
    return (flask.Markup(r'<span class="navbar-text">Logged in as ') +
            user_link(user_name) +
            flask.Markup(r'</span>'))

@app.template_global()
def tooltipAllowsEdits():
    return logged_in_user_name() is not None

def get_entity_labels(entity_list):
    entities = {}
    for entity_group in more_itertools.chunked(entity_list, max_obtainable_entities):
        parameters = {'action': 'wbgetentities',
                      'format': 'json',
                      'ids': '|'.join([entity.replace(wd_prefix,"") for entity in entity_group]),
                      'props': 'labels'}
        headers = {"user-agent": 'namkoron (mahir256 at live.com)'}
        response = requests.get(wikidata_api_url, params=parameters, headers=headers)
        entities.update(response.json()["entities"])
    return entities

def get_label(entity, label_lang):
    label_out = ("", entity["id"])
    if entity["labels"].get(label_lang):
        label_out = (label_lang, entity["labels"][label_lang]["value"])
    elif entity["labels"].get("en"):
        label_out = ("en", entity["labels"]["en"]["value"])
    return label_out

def get_labels(entity_list, label_lang):
    # TODO: use better fallback
    labels = {}
    entity_dict = get_entity_labels(entity_list)
    for qid, entity in entity_dict.items():
        labels[qid] = get_label(entity, label_lang)
    return labels

def process_query_results(results, targetlang):
    # TODO: handle errors and empty queries and no item in result and probably anything else
    test_binding = results[0]
    item_variable_name = next(key for key in test_binding.keys() if (test_binding[key]["type"]=="uri" and wd_prefix in test_binding[key]["value"]))
    coord_variable_name = next(key for key in test_binding.keys() if test_binding[key]["type"]=="literal" and test_binding[key]["datatype"]==wkt_literal_url)
    itemLabel_variable_name = None
    if(item_variable_name + "Label" in test_binding.keys()):
        if(test_binding[item_variable_name + "Label"]["type"] == "literal" and
           "xml:lang" in test_binding[item_variable_name + "Label"]):
            itemLabel_variable_name = item_variable_name + "Label"
    coords = {}
    qid_list = []
    for binding in results:
        qid = binding[item_variable_name]["value"]
        current_coords = coord_pattern.match(binding[coord_variable_name]["value"])
        coords[qid] = [current_coords['lat'], current_coords['lon']]
        if(itemLabel_variable_name is not None):
            current_name = binding[itemLabel_variable_name]
            coords[qid].extend([current_name.get("xml:lang",""), current_name["value"]])
        else:
            qid_list.append(qid)
            if(len(qid_list)==max_obtainable_entities):
                labels = get_labels(qid_list, targetlang)
                for qid in labels:
                    coords[wd_prefix+qid].extend(labels[qid])
                qid_list = []
    if(qid_list != []):
        labels = get_labels(qid_list, targetlang)
        for qid in labels:
            coords[wd_prefix+qid].extend(labels[qid])
    return coords

def submit_label(qid, newlang, newlabel):
    current_session = mwapi.Session(
        host=wikidata_url,
        auth=get_auth(),
        user_agent=user_agent)
    summary = f'via Namkoron (নামকরণ)'
    token = current_session.get(action='query',meta='tokens')['query']['tokens']['csrftoken']
    edit_options = {
        'action': 'wbsetlabel',
        'id': qid,
        'language': newlang,
        'value': newlabel,
        'summary': summary,
        'token': token
    }
    return current_session.post(**edit_options)

def submit_new_label():
    token = flask.session.get('csrf_token', None)
    if (not token or
        token != flask.request.form.get('csrf_token') or
        not flask.request.referrer.startswith(full_url('index'))):
        flask.g.csrf_error = True
        flask.g.repeat_form = True
        return None

    form_data = flask.request.form
    qid = form_data['qid'].replace(wd_prefix,"")
    newlang = form_data['newlang']
    newlabel = form_data['newlabel']
    response = submit_label(qid, newlang, newlabel)
    return response

def run_query(query_string):
    try:
        sparql.setQuery(query_string)
        results = sparql.query().convert()
        return results["results"]["bindings"]
    except Exception as e:
        return {"error":"query"}

def get_custom_query_results():
    token = flask.session.get('csrf_token', None)
    if (not token or
        token != flask.request.form.get('csrf_token') or
        not flask.request.referrer.startswith(full_url('index'))):
        return {"error":"csrf"}

    custom_query = flask.request.form["custom_query"]
    targetlang = flask.request.form["targetlang"]
    if "[AUTO_LANGUAGE]" in custom_query:
        custom_query = custom_query.replace("[AUTO_LANGUAGE]",targetlang)
    results = run_query(custom_query)
    if("error" not in results):
        coords = process_query_results(results, targetlang)
        return coords
    else:
        return results

@app.route('/w.wiki/<shorturl>/<targetlang>', methods=['GET', 'POST'])
def get_query_results(shorturl, targetlang):
    if(flask.request.method == 'POST'):
        if("custom_query" in flask.request.form):
            return get_custom_query_results()
        else:
            return submit_new_label()
    
    query_url = 'https://w.wiki/'+shorturl
    response = requests.get(query_url,allow_redirects=False)
    actual_query_url = response.headers["location"]
    old_query_string = urllib.parse.unquote(actual_query_url.replace(query_embed_prefix,""))
    query_string = old_query_string
    if "[AUTO_LANGUAGE]" in query_string:
        query_string = query_string.replace("[AUTO_LANGUAGE]",targetlang)

    results = run_query(query_string)
    if("error" not in results):
        coords = process_query_results(results, targetlang)
    else:
        coords = process_query_results(results_example, targetlang_example)
    return flask.render_template('main.html',
                                 coords=coords,
                                 target_lang=targetlang,
                                 query_string=old_query_string)

@app.route('/login')
def login():
    redirect, request_token = mwoauth.initiate(wikidata_index_url, consumer_token, user_agent=user_agent)
    flask.session['oauth_request_token'] = dict(zip(request_token._fields, request_token))
    return_url = flask.request.referrer
    if return_url and return_url.startswith(full_url('index')):
        flask.session['oauth_redirect_target'] = return_url
    return flask.redirect(redirect)

@app.route('/oauth/callback')
def oauth_callback():
    request_token = mwoauth.RequestToken(**flask.session['oauth_request_token'])
    access_token = mwoauth.complete(wikidata_index_url, consumer_token, request_token, flask.request.query_string, user_agent=user_agent)
    flask.session['oauth_access_token'] = dict(zip(access_token._fields, access_token))
    return flask.redirect(flask.url_for('index'))

@app.route('/', methods=['GET', 'POST'])
def index():
    if(flask.request.method == 'POST'):
        if("custom_query" in flask.request.form):
            return get_custom_query_results()
        else:
            return submit_new_label()
    coords_example = process_query_results(results_example, targetlang_example)
    return flask.render_template('main.html',
                                 coords=coords_example,
                                 target_lang=targetlang_example,
                                 query_string="SELECT ?i ?iLabel ?cds { ?i wdt:P31/wdt:P279* wd:Q55488 ; wdt:P17 wd:Q668 ; wdt:P625 ?cds . SERVICE wikibase:label { bd:serviceParam wikibase:language '[AUTO_LANGUAGE],en'. } }")

@app.after_request
def denyFrame(response):
    response.headers['X-Frame-Options'] = 'deny'
    return response

